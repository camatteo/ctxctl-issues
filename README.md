# CorTeX-ConTroL issues
Post your issues and bug reports here.

When submitting a bug report, please make sure that you have narrowed down the bug somewhat.
Please report the exact steps required to reproduce the bug.

